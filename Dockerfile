FROM centos:7

RUN yum install -y epel-release \
    && yum install -y python-pip \
    && pip install --no-cache-dir diskimage-builder \
    && yum clean all \
    && rm -rf /var/cache/yum
